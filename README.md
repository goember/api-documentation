# API Documentation

Ember's API documentation. See the current documentation in [production](https://ember-core.stoplight.io/docs/api-documentation).

## Development

1. Download [Stoplight Studio](https://github.com/stoplightio/studio/releases)
2. Open Stoplight Studio and log in using the credentials in [Zoho](https://vault.zoho.eu/app)
3. Open this project in Stoplight Studio
4. Make any changes in Stoplight Studio
    1. If you're adding a new endpoint, make sure to add the `Paths` tag so the endpoint is grouped with all the others
5. Commit the changes that Stoplight Studio has made to the YAML files